FROM python:3.9

#
WORKDIR /code

#EXPOSE 5000 9000

#
RUN pip install --upgrade pip
RUN pip install poetry

#
COPY ./pyproject.toml /code/
COPY ./poetry.lock /code/

COPY ./inference.py /code/app/inference.py
COPY ./.env /code/app/.env


RUN poetry install --no-interaction --no-ansi
  
  
CMD ["poetry", "run", "uvicorn", "app.inference:app", "--host", "0.0.0.0", "--port", "80"]


